from utils import *
from params import params
from theano_model import theano_model
from adagrad import adagrad
import random
import numpy as np
import sys

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.lamWe=float(args[1])
params.L_C = float(args[2])
params.frac = float(args[3])
params.outfile = args[4]
params.dataf = args[5]
params.batchsize = int(args[6])
params.hiddensize = int(args[7])
params.wordfile = args[8]
params.type = args[9]
params.save = True

(words, We) = getWordmap(params.wordfile)
wordfilestem = params.wordfile.split("/")[-1].replace(".txt","")
params.outfile = "../models/"+params.outfile+"."+str(params.lamWe)+"."+str(params.L_C)+"."+str(params.batchsize)+"."+params.type+\
                 "."+wordfilestem+".txt"
examples = getData(params.dataf,words)
word_pairs = getWordPairs('../data/ppdb-new.2.txt',words)
examples = convertPairsToExamples(examples, word_pairs)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lamWe)\
      +" and lambda C ="+str(params.L_C)
print "Saving to: "+params.outfile

adagrad(params, words, theano_model(We,params.hiddensize,params.L_C))
