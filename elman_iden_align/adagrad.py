import numpy as np
from random import shuffle
from utils import getPairs
from evaluate import evaluate_adagrad
from utils import getPairsBatch
from utils import getTuplesFromExamples
import sys
import random
import cPickle

random.seed(1)
np.random.seed(1)

def checkIfQuarter(idx,n,batchsize):
    start = idx - batchsize
    for i in range(start,idx):
        if i==round(n/4.) or i==round(n/2.) or i==round(3*n/4.):
            return True
    return False

def saveParams(words,model,outfile,counter):
    #save as text file for now
    outfile = outfile + ".params."+str(counter)+".pickle"
    fout = open(outfile,'wb')
    data = (words,model)
    cPickle.dump(data, fout, protocol=2)

def evaluateParams(model,words):
    evaluate_adagrad(model,words)

def adagrad(params, words, tm):

    sys.setrecursionlimit(5000)
    data = params.data
    n = len(data)
    counter = 0

    for ep in range(params.epochs):
        shuffle(data)
        pairs = getPairsBatch(data,params.batchsize,params.type, tm)
        #print pairs
        d1,d2,p1,p2,X1,X2 = getTuplesFromExamples(data,pairs)
        #print d1,d2,p1,p2
        cost = tm.getBatchCost(d1,d2,p1,p2,params.lamWe,params.L_C,X1,X2)
        print "cost at epoch at "+str(ep)+" : "+str(cost)
        idx = 0
        while idx < n:
            batch = data[idx: idx + params.batchsize if idx + params.batchsize < len(data) else len(data)]
            idx += params.batchsize
            if(len(batch) <= 2):
                print "batch too small."
                continue #just move on because pairing could go faulty
            pairs = getPairs(batch,params.type, tm)
            d1,d2,p1,p2,X1,X2 = getTuplesFromExamples(batch,pairs)
            tm.updateBatchParams(d1,d2,p1,p2,params.eta,params.lamWe,params.L_C,X1,X2)
            if(checkIfQuarter(idx,n,params.batchsize)):
                if(params.save):
                    counter += 1
                    saveParams(words,tm,params.outfile,counter)
                if(params.evaluate):
                    evaluateParams(tm,words)
                    sys.stdout.flush()
        if(params.save):
            counter += 1
            saveParams(words,tm,params.outfile,counter)
        if(params.evaluate):
            evaluateParams(tm,words)
            sys.stdout.flush()
