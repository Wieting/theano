import theano
import theano.tensor as T
import numpy as np

def np_cosine(x,y):
    nx = np.sqrt(np.sum(x**2))
    ny = np.sqrt(np.sum(y**2))
    return np.dot(x,y)/(nx*ny)

def cosine(x,y):
    nx = T.sqrt(T.sum(x**2))
    ny = T.sqrt(T.sum(y**2))
    return T.dot(x,y)/(nx*ny)

def recurrence(x1,x2,prev_s):
    s = prev_s + cosine(x1,x2)
    return s

X1 = T.dmatrix()
X2 = T.dmatrix()
l = T.dscalar()

def ff(X1,X2):
    zero = theano.shared(0.)
    s, _ = theano.scan(fn=recurrence, sequences=[X1,X2], outputs_info=[zero],n_steps=X1.shape[0])
    return s[-1]/X1.shape[0]

f = theano.function(inputs = [X1,X2], outputs = ff(X1,X2))

X = [[1,2],[3,4],[7,2]]
Y = [[.1,.3],[.5,.2],[.6,.9]]

print f(X,Y)

tot=0.
for i in range(len(X)):
    tot += np_cosine(np.array(X[i]),np.array(Y[i]))
print tot/3
