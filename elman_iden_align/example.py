from tree import tree

class example(object):

    def __init__(self, tree1, tree2):

        self.tree1 = tree1
        self.tree2 = tree2
        self.words1 = []
        self.words2 = []

    def __str__(self):
        return self.tree1.__str__()+"\t"+self.tree2.__str__()

    def populate_aligned_words(self, wordpairs):
        for i in self.tree1.embeddings:
            if i in wordpairs:
                for j in self.tree2.embeddings:
                    if j in wordpairs[i]:
                        self.words1.append(i)
                        self.words2.append(j)
        #print self.words1, self.words2
