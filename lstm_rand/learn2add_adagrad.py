import numpy as np
from random import shuffle
from utils import getPairs
from evaluate import evaluate_adagrad
from utils import getPairsBatch
from utils import getTuplesFromExamples
import sys
import random
import cPickle

random.seed(1)
np.random.seed(1)

def checkIfQuarter(idx,n,batchsize):
    start = idx - batchsize
    for i in range(start,idx):
        if i==round(n/4.) or i==round(n/2.) or i==round(3*n/4.):
            return True
    return False

def saveParams(model,outfile,counter):
    #save as text file for now
    outfile = outfile + ".params."+str(counter)+".pickle"
    fout = open(outfile,'wb')
    data = model
    cPickle.dump(data, fout, protocol=2)

def evaluateParams(model,Y):
    c = 0
    for i in Y:
        c += model.evaluatef(i[0],i[1])
    c /= len(Y)
    print "Test score: {0:.4f}".format(c)

def adagrad(params, tm, eta, Y):

    sys.setrecursionlimit(5000)
    data = params.data
    n = len(data)
    counter = 0

    for ep in range(params.epochs):
        shuffle(data)
        #print d1,d2,p1,p2
        cost = tm.getBatchCost(data,params.L_C)
        print "cost at epoch at "+str(ep)+" : "+str(cost)
        idx = 0
        while idx < n:
            batch = data[idx: idx + params.batchsize if idx + params.batchsize < len(data) else len(data)]
            idx += params.batchsize
            if(len(batch) <= 2):
                print "batch too small."
                continue #just move on because pairing could go faulty
            tm.updateBatchParams(data,eta,params.L_C)
            if(checkIfQuarter(idx,n,params.batchsize)):
                if(params.save):
                    counter += 1
                    saveParams(tm,params.outfile,counter)
                if(params.evaluate):
                    evaluateParams(tm,Y)
                    sys.stdout.flush()
        if(params.save):
            counter += 1
            saveParams(tm,params.outfile,counter)
        if(params.evaluate):
            evaluateParams(tm,Y)
            sys.stdout.flush()
