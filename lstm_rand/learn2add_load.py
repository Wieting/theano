
from params import params
from learn2add_lstm import learn2add_lstm
from learn2add_adagrad import adagrad
import random
import numpy as np
import cPickle as pickle

random.seed(1)
np.random.seed(1)

model = pickle.load(open('test.out.params.16.pickle','rb'))

#print model.params[1].get_value()

N = 10000
data = []

for i in range(N):
    l = random.randint(3,5)
    m = np.random.uniform(low=-2, high = 2, size=(l,25))
    y = m.sum(axis=0)
    y = y / np.sqrt(np.dot(y,y))
    data.append((m,y))

train = data[0:int(0.9*N)]
test = data[int(0.9*N):N]

for i in test:
    print i[1][0:3], model.feedforwardf(i[0])[0:3]
    print model.evaluatef(i[0],i[1])