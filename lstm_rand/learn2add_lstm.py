
import theano
import numpy as np
from theano import tensor as T
import pdb
from theano.ifelse import ifelse
from collections import OrderedDict
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

class learn2add_lstm(object):

    def initWeights(self, D) :
        Wxi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bi = theano.shared(np.zeros(D))

        Wxj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bj = theano.shared(np.zeros(D))

        Wxk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bk = theano.shared(np.zeros(D))

        Wxo = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Who = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bo = theano.shared(np.zeros(D))

        h0 = theano.shared(np.zeros(D))
        c0 = theano.shared(np.zeros(D))

        return  (Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

    def rnn_layer(self, X, params):
        (Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = params

        def recurrence(x_t, h_tm1, c_tm1):
            it = T.nnet.sigmoid(T.dot(Wxi, x_t) + T.dot(Whi, h_tm1) + bi)
            ft = T.nnet.sigmoid(T.dot(Wxj, x_t) + T.dot(Whj, h_tm1) + bj)
            ot = T.nnet.sigmoid(T.dot(Wxk, x_t) + T.dot(Whk, h_tm1) + bk)
            ut = T.tanh(T.dot(Wxo,x_t)+T.dot(Who,h_tm1) + bo)

            ct = it*ut+ft*c_tm1
            ht = ot*T.tanh(ct)
            return ht,ct

        rep, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0,c0],
                                n_steps=X.shape[0])
        return rep[0][-1]

    def cosine(self,x,y):
        nx = T.sqrt(T.sum(x**2))
        ny = T.sqrt(T.sum(y**2))
        return T.dot(x,y)/(nx*ny)
    
    def L2(self,L2_reg,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14):
        return L2_reg/2. * ((w1 ** 2).sum() + (w2 ** 2).sum() + (w3 ** 2).sum()
                            + (w4 ** 2).sum() + (w5 ** 2).sum() + (w6 ** 2).sum()
                            + (w7 ** 2).sum() + (w8 ** 2).sum() + (w9 ** 2).sum()
                            + (w10 ** 2).sum() + (w11 ** 2).sum() + (w12 ** 2).sum()
                            + (w13 ** 2).sum() + (w14 ** 2).sum())

    def obj(self,X,Y):
        return abs((X-Y)).sum()

    def __init__(self, D, L_C):

        self.L_C = L_C
        self.D = D
        self.rng = np.random.RandomState(1); self.srng = RandomStreams(self.rng.randint(99999))

        (Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = self.initWeights(D)
        self.params = (Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

        gWxi = T.dmatrix(); gWhi = T.dmatrix(); gbi = T.dvector();
        gWxj = T.dmatrix(); gWhj = T.dmatrix(); gbj = T.dvector();
        gWxk = T.dmatrix(); gWhk = T.dmatrix(); gbk = T.dvector();
        gWxo = T.dmatrix(); gWho = T.dmatrix(); gbo = T.dvector();
        gh0 = T.dvector(); gc0 = T.dvector()
        self.gparams = [gWxi,gWhi,gbi, gWxj,gWhj,gbj, gWxk,gWhk,gbk, gWxo,gWho,gbo, gh0,gc0]

        #cost and gradient
        L_C = T.dscalar()

        I = T.dmatrix()
        X = self.rnn_layer(I,self.params)
        Y = T.dvector()

        self.feedforwardf = theano.function(inputs = [I],  outputs = X)
        cost1 = self.obj(X,Y)
        cost2 = L_C/2.*self.L2(L_C,Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)
        
        self.cost_f1 = theano.function(inputs = [I,Y], outputs = cost1)
        self.cost_f2 = theano.function(inputs = [L_C], outputs = cost2)
        self.gradf1 = theano.function(inputs=[I,Y], outputs = T.grad(cost1, self.params))
        self.gradf2 = theano.function(inputs=[L_C], outputs = T.grad(cost2, self.params))

        self.evaluatef = theano.function(inputs = [I,Y], outputs = cost1)

        #updates
        eta = T.dscalar()
    
        updates = OrderedDict()
        for p,gp in zip(self.params,self.gparams):
            agrad = theano.shared(np.zeros(p.get_value().shape, dtype=theano.config.floatX))
            agrad_new = agrad + gp*gp
            updates[agrad] = agrad_new
            updates[p]=p-(eta/T.sqrt(agrad_new+1E-6))*gp

        self.updatesf = theano.function(inputs=self.gparams+[eta], updates=updates)


    def updateBatchParams(self,data,eta,L_C):
        gg = [0.]*len(self.params)
        for i in range(len(data)):
            gg = map(sum, zip(gg, self.gradf1(data[i][0],data[i][1])))
        gg = map(lambda a: a/len(data), gg)
        gg = map(sum, zip(gg, self.gradf2(L_C)))
        args = gg + [eta]
        self.updatesf(*args)

    def getBatchCost(self,data,L_C):
        cost = 0
        for i in range(len(data)):
            cost += self.cost_f1(data[i][0],data[i][1])
        return cost/len(data) + self.cost_f2(L_C)
