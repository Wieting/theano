
import theano
import numpy as np
from theano import tensor as T
import pdb
from theano.ifelse import ifelse
from collections import OrderedDict
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams
import cPickle as pickle

class theano_lstm_model(object):

    def initWeights(self, D, We_initial) :
        We = theano.shared(We_initial)

        Wxi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxo = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Who = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bo = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        h0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))
        c0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        return  (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

    def initWeightsFromModel(self, model, We_initial):
        model = pickle.load(open('test.out.params.16.pickle','rb'))

        We = theano.shared(We_initial)

        Wxi = theano.shared(model.params[0].get_value())
        Whi = theano.shared(model.params[1].get_value())
        bi = theano.shared(model.params[2].get_value())

        Wxj = theano.shared(model.params[3].get_value())
        Whj = theano.shared(model.params[4].get_value())
        bj = theano.shared(model.params[5].get_value())

        Wxk = theano.shared(model.params[6].get_value())
        Whk = theano.shared(model.params[7].get_value())
        bk = theano.shared(model.params[8].get_value())

        Wxo = theano.shared(model.params[9].get_value())
        Who = theano.shared(model.params[10].get_value())
        bo = theano.shared(model.params[11].get_value())

        h0 = theano.shared(model.params[12].get_value())
        c0 = theano.shared(model.params[13].get_value())

        return  (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

    def rnn_layer(self, I, params):
        (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = params
        X = We[I]
        def recurrence(x_t, h_tm1, c_tm1):
            it = T.nnet.sigmoid(T.dot(Wxi, x_t) + T.dot(Whi, h_tm1) + bi)
            ft = T.nnet.sigmoid(T.dot(Wxj, x_t) + T.dot(Whj, h_tm1) + bj)
            ot = T.nnet.sigmoid(T.dot(Wxk, x_t) + T.dot(Whk, h_tm1) + bk)
            ut = T.tanh(T.dot(Wxo,x_t)+T.dot(Who,h_tm1) + bo)

            ct = it*ut+ft*c_tm1
            ht = ot*T.tanh(ct)
            return ht,ct

        rep, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0,c0],
                                n_steps=X.shape[0])
        return rep[0][-1]

    def cosine(self,x,y):
        nx = T.sqrt(T.sum(x**2))
        ny = T.sqrt(T.sum(y**2))
        return T.dot(x,y)/(nx*ny)
    
    def L2(self,L2_reg,w1,w2,w3,w4,w5,w6,w7,w8,w9,w10,w11,w12,w13,w14):
        return L2_reg/2. * ((w1 ** 2).sum() + (w2 ** 2).sum() + (w3 ** 2).sum()
                            + (w4 ** 2).sum() + (w5 ** 2).sum() + (w6 ** 2).sum()
                            + (w7 ** 2).sum() + (w8 ** 2).sum() + (w9 ** 2).sum()
                            + (w10 ** 2).sum() + (w11 ** 2).sum() + (w12 ** 2).sum()
                            + (w13 ** 2).sum() + (w14 ** 2).sum())


    def hinge_obj(self,R_1,R_2,P_1,P_2):
        zero = theano.shared(0.)
        hinge1 = 1-T.dot(R_1[-1],R_2[-1])+T.dot(R_1[-1],P_1[-1])
        hinge2 = 1-T.dot(R_1[-1],R_2[-1])+T.dot(R_2[-1],P_2[-1])
        d1 = ifelse(T.lt(hinge1,zero), zero, hinge1)
        d2 = ifelse(T.lt(hinge2,zero), zero, hinge2)
        return d1 + d2

    def __init__(self, We_initial, D, L_C):

        self.L_C = L_C
        self.D = D
        self.rng = np.random.RandomState(1); self.srng = RandomStreams(self.rng.randint(99999))

        #params
        initial_We = theano.shared(We_initial)
        #experimental.....
        #(We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = self.initWeights(D, We_initial)
        #self.params = (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

        model = pickle.load(open('test.out.params.16.pickle','rb'))
        (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = self.initWeightsFromModel(model, We_initial)
        self.params = (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)
        #done with experimental.....

        gWe = T.dmatrix();
        gWxi = T.dmatrix(); gWhi = T.dmatrix(); gbi = T.dvector();
        gWxj = T.dmatrix(); gWhj = T.dmatrix(); gbj = T.dvector();
        gWxk = T.dmatrix(); gWhk = T.dmatrix(); gbk = T.dvector();
        gWxo = T.dmatrix(); gWho = T.dmatrix(); gbo = T.dvector();
        gh0 = T.dvector(); gc0 = T.dvector()
        self.gparams = [gWe, gWxi,gWhi,gbi, gWxj,gWhj,gbj, gWxk,gWhk,gbk, gWxo,gWho,gbo, gh0,gc0]

        #feed forward
        I = T.ivector()
        self.feedforwardf = theano.function(inputs = [I], outputs = self.rnn_layer(I,self.params))

        #cost and gradient
        I_1 = T.ivector(); I_2 = T.ivector(); I_3 = T.ivector(); I_4 = T.ivector()
        L_We = T.dscalar(); L_C = T.dscalar()

        R_1 = self.rnn_layer(I_1,self.params); R_2 = self.rnn_layer(I_2,self.params)
        P_1 = self.rnn_layer(I_3,self.params); P_2 = self.rnn_layer(I_4,self.params)

        cost1 = self.hinge_obj(R_1,R_2,P_1,P_2)
        cost2 = L_C/2.*self.L2(L_C,Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) \
            + L_We/2.*T.sum((We-initial_We)**2)
        
        self.cost_f1 = theano.function(inputs = [I_1,I_2,I_3,I_4], outputs = cost1)
        self.cost_f2 = theano.function(inputs = [L_We,L_C], outputs = cost2)
        self.gradf1 = theano.function(inputs=[I_1,I_2,I_3,I_4], outputs = T.grad(cost1, self.params))
        self.gradf2 = theano.function(inputs=[L_We,L_C], outputs = T.grad(cost2, self.params))

        #updates
        eta = T.dscalar()
    
        updates = OrderedDict()
        for p,gp in zip(self.params,self.gparams):
            agrad = theano.shared(np.zeros(p.get_value().shape, dtype=theano.config.floatX))
            agrad_new = agrad + gp*gp
            updates[agrad] = agrad_new
            updates[p]=p-(eta/T.sqrt(agrad_new+1E-6))*gp

        self.updatesf = theano.function(inputs=self.gparams+[eta], updates=updates)


    def feedforward(self, xx):
        repr = self.feedforwardf(xx)
        return repr

    def feedforwardAll(self, batch):
        for i in batch:
            t1 = i[0]
            t2 = i[1]
            #print t1.embeddings
            t1.representation = self.feedforward(t1.embeddings)
            t2.representation = self.feedforward(t2.embeddings)

    def getVectorFromIndices(self, idx1, idx2):
        r1 = self.feedforward(idx1)
        r2 = self.feedforward(idx2)
        return r1,r2

    def updateBatchParams(self,d1,d2,p1,p2,eta,lamWe,L_C):
        gg = [0.]*len(self.params)
        for i in range(len(d1)):
            gg = map(sum, zip(gg, self.gradf1(d1[i],d2[i],p1[i],p2[i])))
        gg = map(lambda a: a/len(d1), gg)
        gg = map(sum, zip(gg, self.gradf2(lamWe,L_C)))
        args = gg + [eta]
        self.updatesf(*args)

    def getBatchCost(self,d1,d2,p1,p2,lamWe,L_C):
        cost = 0
        for i in range(len(d1)):
            cost += self.cost_f1(d1[i], d2[i], p1[i], p2[i])
        return cost/len(d1) + self.cost_f2(lamWe,L_C)
