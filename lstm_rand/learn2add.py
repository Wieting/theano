
from params import params
from learn2add_lstm import learn2add_lstm
from learn2add_adagrad import adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.LC=1E-6
params.frac = 0.1
params.outfile = 'test.out'
params.batchsize = 100
params.hiddensize = 25
params.save = True

#make training data - matrices of lengths 3 to 5, ys are sum

N = 10000
data = []

for i in range(N):
    l = random.randint(3,5)
    m = np.random.uniform(low=-2, high = 2, size=(l,params.hiddensize))
    y = m.sum(axis=0)
    y = y / np.sqrt(np.dot(y,y))
    data.append((m,y))

train = data[0:int(0.9*N)]
test = data[int(0.9*N):N]

params.data = train
model = learn2add_lstm(params.hiddensize,params.LC)
adagrad(params,model,params.eta,test)