from utils import lookup
from utils import getWordmap
from params import params
from utils import getData
from utils import getPairsBatch
from objective import objective
import random
import numpy as np

random.seed(1)
np.random.seed(1)
hiddensize = 25
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
examples = getData(params.dataf)
examples = examples[0:20]
params.batchsize=5

Wi = 0.2 * np.random.uniform(-1.0, 1.0,(hiddensize, hiddensize))
bi = np.zeros(hiddensize)
W1  = 0.2 * np.random.uniform(-1.0, 1.0,(hiddensize, hiddensize))
W2  = 0.2 * np.random.uniform(-1.0, 1.0,(hiddensize, hiddensize))
b  = np.zeros(hiddensize)

#print Wi, bi, W1, W2, b

for (t1,t2,s) in examples:
    t1.feedforward(Wi, bi, W1, W2, b, We, words)
    t2.feedforward(Wi, bi, W1, W2, b, We, words)
    #print t1.representation

pairs = getPairsBatch(examples,words,We,5)
cost = objective(hiddensize,examples,pairs,words,We,We,Wi,bi,W1,W2,b,params)
print cost
