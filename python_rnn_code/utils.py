from scipy.io import loadmat
import numpy as np
from tree import tree
from random import shuffle

def lookup(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:]
    else:
        return We[words['UUUNKKK'],:]

def lookup_with_unk(We,words,w):
    w = w.lower()
    if w in words:
        return We[words[w],:],False
    else:
        return We[words['UUUNKKK'],:],True

#changed
def getData(f):
    data = open(f,'r')
    lines = data.readlines()
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split('|||')
            e = (tree(i[0]), tree(i[1]), float(i[2]))
            examples.append(e)
    return examples

def getWordmap(textfile):
    words={}
    We = []
    f = open(textfile,'r')
    lines = f.readlines()
    for (n,i) in enumerate(lines):
        i=i.split()
        j = 1
        v = []
        while j < len(i):
            v.append(float(i[j]))
            j += 1
        words[i[0]]=n
        We.append(v)
    return (words, np.array(We))

def getPair(label,vec,idx,d,We,words):
    min = -5000
    max = 5000
    best = -2
    for i in range(len(d)):
        if i == idx:
            continue
        (w1,w2,l) = d[i]
        v1 = w1.representation
        v2 = w2.representation
        np1 = np.inner(v1,vec)
        np2 = np.inner(v2,vec)
        if(label > 0):
            if(np1 > min):
                min = np1
                best = w1
            if(np2 > min):
                min = np2
                best = w2
        else:
            if(np1 < max):
                max = np1
                best = w1
            if(np2 < max):
                max = np2
                best = w2
    return best

def getPairs(d, words, We):
    pairs = []
    for i in range(len(d)):
        (w1,w2,l) = d[i]
        v1 = w1.representation
        v2 = w2.representation
        pairs.append((getPair(l,v1,i,d,We,words),getPair(l,v2,i,d,We,words)))
    return pairs

def getPairsBatch(d, words, We, batchsize):
    idx = 0
    pairs = []
    while idx < len(d):
        batch = d[idx: idx + batchsize if idx + batchsize < len(d) else len(d)]
        if(len(batch) <= 2):
            print "batch too small."
            continue #just move on because pairing could go faulty
        p = getPairs(batch,words,We)
        pairs.extend(p)
        idx += batchsize
    return pairs



