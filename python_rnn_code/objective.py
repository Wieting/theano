import numpy as np
import pdb

def objective(hiddensize, d, pairs, words, We, We_old, Wi, bi, W1, W2, b, params):
    (a, b) = We.shape
    margin = params.margin

    if a==1 or b == 1:
        n = We.size
        v = int(n/hiddensize)
        We = np.reshape(We,(v,hiddensize))

    total = 0.
    zero = 0.

    for ii in range(len(d)):
        t1 = d[ii][0]
        t2 = d[ii][1]
        label = d[ii][2]
    
        p1 = pairs[ii][0]
        p2 = pairs[ii][1]
    
        g1 = t1.representation
        g2 = t2.representation
        v1 = p1.representation
        v2 = p2.representation

        if(label > 0):
            d1 = margin - np.dot(g1,g2) + np.dot(v1, g1)
            d2 = margin - np.dot(g1,g2) + np.dot(v2, g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0
            total = total + d1 + d2
        else:
            d1 = margin + np.dot(g1,g2) + np.dot(v1, g1)
            d2 = margin + np.dot(g1,g2) + np.dot(v2, g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0
            total = total + d1 + d2

    total = total / len(d)
    total2 = np.power(We_old-We,2).sum() * params.lamC / 2
    cost = total+total2
    return cost


