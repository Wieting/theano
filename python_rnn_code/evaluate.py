from scipy.spatial.distance import cosine
from scipy.stats import spearmanr
from utils import lookup_with_unk
from utils import lookup
from utils import getWordmap
import numpy as np

def getwordsim(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[2]))
            examples.append(ex)
    return examples

def getsimlex(file):
    file = open(file,'r')
    lines = file.readlines()
    lines.pop(0)
    examples = []
    for i in lines:
        i=i.strip()
        if(len(i) > 0):
            i=i.split()
            ex = (i[0],i[1],float(i[3]))
            examples.append(ex)
    return examples

def getCorr(examples, We, words):
    gold = []
    pred = []
    num_unks = 0
    for i in examples:
        (v1,t1) = lookup_with_unk(We,words,i[0])
        (v2,t2) = lookup_with_unk(We,words,i[1])
        #print v1,v2
        pred.append(-1*cosine(v1,v2)+1)
        if t1:
            num_unks += 1
            #print i[0]
        if t2:
            num_unks += 1
        gold.append(i[2])
    return (spearmanr(pred,gold)[0], num_unks)

def evaluateWordSim(We, words):
    ws353ex = getwordsim('../dataset/wordsim353.txt')
    ws353sim = getwordsim('../dataset/wordsim-sim.txt')
    ws353rel = getwordsim('../dataset/wordsim-rel.txt')
    simlex = getsimlex('../dataset/SimLex-999.txt')
    (c1,u1) = getCorr(ws353ex,We,words)
    (c2,u2) = getCorr(ws353sim,We,words)
    (c3,u3) = getCorr(ws353rel,We,words)
    (c4,u4) = getCorr(simlex,We,words)
    return ([c1,c2,c3,c4],[u1,u2,u3,4])

def llm(p1,p2,words,We):
    p1 = p1.split()
    p2 = p2.split()
    total = 0
    for i in p1:
        v1 = lookup(We,words,i)
        max = 0
        for j in p2:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score = 0.5*total / len(p1)
    total = 0
    for i in p2:
        v1 = lookup(We,words,i)
        max = 0
        for j in p1:
            v2 = lookup(We,words,j)
            score = -1*cosine(v1,v2)+1
            if(score > max):
                max = score
        total += max
    llm_score += 0.5*total / len(p2)
    return llm_score

def add(p1,p2,words,We):
    p1 = p1.split()
    p2 = p2.split()
    accumulator = np.zeros(lookup(We,words,p1[0]).shape)
    for i in p1:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p1_emb = accumulator / len(p1)
    accumulator = np.zeros(lookup(We,words,p2[0]).shape)
    for i in p2:
        v = lookup(We,words,i)
        accumulator = accumulator + v
    p2_emb = accumulator / len(p1)
    return -1*cosine(p1_emb,p2_emb)+1


def scoreannoppdb(f,We,words):
    f = open(f,'r')
    lines = f.readlines()
    examples=[]
    llm_preds = []
    add_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.split('|||')
        (p1,p2,score) = (i[0].strip(),i[1].strip(),float(i[2]))
        llm_preds.append(llm(p1,p2,words,We))
        add_preds.append(add(p1,p2,words,We))
        gold.append(score)
    return (spearmanr(llm_preds,gold)[0], spearmanr(add_preds,gold)[0])

def scoreSE(f,We,words):
    f = open(f,'r')
    lines = f.readlines()
    lines.pop(0)
    examples=[]
    llm_preds = []
    add_preds = []
    gold = []
    for i in lines:
        i=i.strip()
        i=i.split('\t')
        (p1,p2,score) = (i[1].strip(),i[2].strip(),float(i[3]))
        llm_preds.append(llm(p1,p2,words,We))
        add_preds.append(add(p1,p2,words,We))
        gold.append(score)
    return (spearmanr(llm_preds,gold)[0], spearmanr(add_preds,gold)[0])

def evaluateAnno(We, words):
    return scoreannoppdb('../dataset/ppdb_test.txt',We,words)

def evaluateSE(We, words):
    return scoreSE('../dataset/semeval_test.txt',We,words)

def evaluate_adagrad(We,words):
    (corr, unk) = evaluateWordSim(We,words)
    (llm_s, add_s) = evaluateAnno(We,words)
    s = "{0} {1} {2} {3} {4} {5} ws353 sim rel sl999 ppdb_llm ppdb_add".format(corr[0], corr[1], corr[2], corr[3], llm_s, add_s)
    print s

if __name__ == "__main__":
    # (words, We) = getWordmap('../data/skipwiki25.txt')
    # (words, We) = getWordmap('../data/paragram_vectors.txt')
    # (corr, unk) = evaluateWordSim(We,words)
    # (llm_s1, add_s1) = evaluateAnno(We,words)
    # (llm_s2, add_s2) = evaluateSE(We,words)
    # print corr, unk
    # print llm_s1, add_s1
    # print llm_s2, add_s2
    #
    # print ""
    #
    # (words, We) = getWordmap('../data/skipwiki25.txt')
    # (corr, unk) = evaluateWordSim(We,words)
    # (corr, unk) = evaluateWordSim(We,words)
    # (llm_s1, add_s1) = evaluateAnno(We,words)
    # (llm_s2, add_s2) = evaluateSE(We,words)
    # print corr, unk
    # print llm_s1, add_s1
    # print llm_s2, add_s2
    (words, We) = getWordmap('../data/skipwiki25.txt')
    evaluate_adagrad(We,words)

