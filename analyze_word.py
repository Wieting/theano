from glob import glob

flist = glob("training_words/SGE/sh.o*")

def getScore(funcOnArrayEvaluate, funcOnArrayKeep):
    dd = {}

    for i in flist:
        i=open(i,'r')
        lines = i.readlines()
        lam=-1
        data=-1
        batch=-1
        method="NA"
        name = ""
        best=-1; keep=-1; best_s=None
        for i in lines:
            if 'command:' in i:
                arr = i.split()
                lam = arr[3]
                batch = arr[-3]
                data = arr[5]
                method = arr[-1]
                name = method+" "+data+" "+batch+" "+lam
            if 'wsim' in i:
                arr = i.split()
                t = funcOnArrayEvaluate(arr)
                if t > best:
                    best_s = i
                    best = t
                    keep = funcOnArrayKeep(arr)
                    dd[name] = (best,keep)

    #sort dicts
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

    for i in dd_sorted:
        print i[0]+"\t"+str(i[1][1])

print "oracle sl"
f = lambda x: float(x[4])
getScore(f,f)
print "\nsl test"
f1 = lambda x: float(x[3])*2 - float(x[2])
f2 = lambda x: float(x[4])
getScore(f1,f2)
print "\nllm"
f = lambda x: float(x[6])
getScore(f,f)
print "\nadd"
f = lambda x: float(x[7])
getScore(f,f)
print "\nSemEval"
f = lambda x: float(x[11])
getScore(f,f)
print "\nJHU word"
f = lambda x: float(x[-10])
getScore(f,f)
print "\nJHU add"
f = lambda x: float(x[-6])
getScore(f,f)
print ""
