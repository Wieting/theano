from glob import glob

def getScore(funcOnArrayEvaluate, funcOnArrayKeep):
    dd = {}

    for j in flist:
        f=open(j,'r')
        lines = f.readlines()
        lam=-1
        lamWe=-1
        batch=-1
        name = ""
        best=-1; keep=-1; best_s=None
        for i in lines:
            if 'command:' in i:
                arr = i.split()
                lamWe = arr[3]
                lam = arr[4]
                batch = arr[-4]
                name = lam+" "+lamWe+" "+batch+" MAX"
            if 'ppdb_dev' in i:
                arr = i.split()
                t = funcOnArrayEvaluate(arr)
                if t > best:
                    best_s = i
                    best = t
                    keep = funcOnArrayKeep(arr)
                    dd[name] = (best,keep)

    #sort dicts
    dd_sorted=sorted(dd.items(), key=lambda x: x[1][0], reverse=True)

    for i in dd_sorted:
        print i[0] +"\t" +str(i[1][1])

flist = glob("training_phrases/SGE/sh.o*")
print "oracle ppdb"
f = lambda x: float(x[1])
getScore(f,f)
print "\nppdb test"
f1 = lambda x: float(x[0])
f2 = lambda x: float(x[1])
getScore(f1,f2)
print "\nse"
f = lambda x: float(x[3])
getScore(f,f)
print "\nJHU"
f = lambda x: float(x[5])
getScore(f,f)