import numpy as np

def objective(hiddensize, d, pairs, words, We, We_old, lam, margin ):
    (a, b) = We.shape

    if a==1 or b == 1:
        n = We.size
        v = int(n/hiddensize)
        We = np.reshape(We,(v,hiddensize))

    total = 0

    for ii in range(len(d)):
        t1 = d[ii][0]
        t2 = d[ii][1]
        label = d[ii][2]
    
        p1 = pairs[ii][0]
        p2 = pairs[ii][1]
    
        g1 = We[words[t1],:]
        g2 = We[words[t2],:]
        v1 = We[words[p1],:]
        v2 = We[words[p2],:]

        if(label > 0):
            d1 = margin - np.inner(g1,g2) + np.inner(v1, g1)
            d2 = margin - np.inner(g1,g2) + np.inner(v2, g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0
            total = total + d1 + d2
        else:
            d1 = margin + np.inner(g1,g2) + np.inner(v1, g1)
            d2 = margin + np.inner(g1,g2) + np.inner(v2, g2)
            if(d1 < 0):
                d1 = 0
            if(d2 < 0):
                d2 = 0
            total = total + d1 + d2

    total = total / len(d)
    total2 = np.power(We_old-We,2).sum() * lam / 2
    cost = total+total2
    return float(cost)


