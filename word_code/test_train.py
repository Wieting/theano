import sys
import warnings
from utils import getWordmap
from params import params
from utils import getData
from adagrad import adagrad
from theano_model_batch import train
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()

params.lam= 1E-5
params.frac = 1.0
params.outfile = 'test.out'
params.dataf = '../data/ppdb-new.2.txt'
params.batchsize = 100
params.hiddensize = 25
#params.type = "MIX"
params.type = "MAX"
params.save = False
params.evaluate = False

(words, We) = getWordmap('../data/skipwiki25.txt')
#(words, We) = getWordmap('../data/paragram300.txt')
params.outfile = "../models/"+params.outfile+"."+str(params.lam)+"."+str(params.batchsize)+"."+params.type+".txt"
examples = getData(params.dataf)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lam)
print "Saving models to: "+params.outfile

train(params, words, We)