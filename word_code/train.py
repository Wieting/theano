import sys
from utils import getWordmap
from params import params
from utils import getData
from theano_model_batch import train
import random
import numpy as np

random.seed(1)
np.random.seed(1)

params = params()
args = sys.argv

params.lam=float(args[1])
params.frac = float(args[2])
params.outfile = args[3]
params.dataf = args[4]
params.batchsize = int(args[5])
params.margin = float(args[6])
params.hiddensize = int(args[7])
params.type = args[8]
params.save = True

(words, We) = getWordmap('../data/paragram300.txt')
#(words, We) = getWordmap('../data/skipwiki25.txt')
params.outfile = "../models/"+params.outfile+"."+str(params.lam)+"."+str(params.batchsize)+"."+params.type+".txt"
examples = getData(params.dataf)

params.data = examples[0:int(params.frac*len(examples))]

print "Training on "+str(len(params.data))+" examples using lambda="+str(params.lam)
print "Saving models to: "+params.outfile

train(params, words, We)
