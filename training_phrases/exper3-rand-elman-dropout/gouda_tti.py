import sys
import os

data = sys.argv[1]
fname = sys.argv[2]
frac = sys.argv[3]
wordfile = sys.argv[4]

lambdaWe=[10,.1,.001,.00001,.0000001]
drop = [0.3,0.5,0.7]
bs=[50,100,200,500]
type = ["MAX"]

for j in range(len(bs)):
    for i in range(len(lambdaWe)):
        for k in range(len(type)):
            for l in range(len(drop)):
                vars = (lambdaWe[i],drop[l],frac,fname,data,bs[j],25,wordfile,type[k])
                cmd = "sh exp3.train_gouda.sh {0} {1} {2} {3} {4} {5} {6} {7} {8}".format(*vars)
                print cmd
