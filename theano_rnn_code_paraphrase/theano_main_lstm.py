
from utils import getWordmap
from params import params
from utils import getData
from theano_model_dropout import theano_model_dropout
from utils import getPairsBatch
from utils import getTuplesFromExamples
from random import shuffle
from adagrad import adagrad
from evaluate import evaluate_adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)

D = 25
lamC = 1E-2
lamWe = 1E-5
eta = 0.01
type = "MAX"
(words, We) = getWordmap('../data/skipwiki25.txt')
params = params()
params.lamC = lamC
params.lamWe = lamWe
examples = getData(params.dataf, words)
examples = examples[0:1000]
params.batchsize=100

#model.feedforwardAll(examples)
#pairs = getPairsBatch(examples,params.batchsize,type)

#d1,d2,p1,p2 = getTuplesFromExamples(examples,pairs)

#print model.getBatchCost(d1,d2,p1,p2,lamC,lamWe)
#model.updateBatchParams(d1,d2,p1,p2,eta,lamC,lamWe)
#print model.getBatchCost(d1,d2,p1,p2,lamC,lamWe)

params.data = examples
adagrad(params, We, D, words, theano_model_dropout(We,D,0.8))
#evaluate_adagrad(model,words)