
import theano
import numpy as np
from theano import tensor as T
import pdb
from theano.ifelse import ifelse

class theano_model(object):

    def initWeights(self, D, We_initial) :
        We = theano.shared(We_initial)

        Wxi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bi = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bj = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Whk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bk = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wxo = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Who = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        bo = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        h0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))
        c0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        return  (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

    def initAda(self, params):
        (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = params
        We_ada = theano.shared(value=np.zeros(We.get_value().shape, dtype=theano.config.floatX))
        Wxi_ada = theano.shared(value=np.zeros(Wxi.get_value().shape, dtype=theano.config.floatX))
        Whi_ada = theano.shared(value=np.zeros(Whi.get_value().shape, dtype=theano.config.floatX))
        bi_ada = theano.shared(value=np.zeros(bi.get_value().shape, dtype=theano.config.floatX))

        Wxj_ada = theano.shared(value=np.zeros(Wxj.get_value().shape, dtype=theano.config.floatX))
        Whj_ada = theano.shared(value=np.zeros(Whj.get_value().shape, dtype=theano.config.floatX))
        bj_ada = theano.shared(value=np.zeros(bj.get_value().shape, dtype=theano.config.floatX))

        Wxk_ada = theano.shared(value=np.zeros(Wxk.get_value().shape, dtype=theano.config.floatX))
        Whk_ada = theano.shared(value=np.zeros(Whk.get_value().shape, dtype=theano.config.floatX))
        bk_ada = theano.shared(value=np.zeros(bk.get_value().shape, dtype=theano.config.floatX))

        Wxo_ada = theano.shared(value=np.zeros(Wxo.get_value().shape, dtype=theano.config.floatX))
        Who_ada = theano.shared(value=np.zeros(Who.get_value().shape, dtype=theano.config.floatX))
        bo_ada = theano.shared(value=np.zeros(bo.get_value().shape, dtype=theano.config.floatX))

        h0_ada = theano.shared(value=np.zeros(h0.get_value().shape, dtype=theano.config.floatX))
        c0_ada = theano.shared(value=np.zeros(c0.get_value().shape, dtype=theano.config.floatX))

        return (We_ada, Wxi_ada,Whi_ada,bi_ada, Wxj_ada,Whj_ada,bj_ada, Wxk_ada,
                Whk_ada,bk_ada, Wxo_ada, Who_ada, bo_ada, h0_ada, c0_ada)

    def rnn_layer(self, I, params):
        (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = params
        X = We[I]
        def recurrence(x_t, h_tm1, c_tm1):
            it = T.tanh(T.dot(Wxi, x_t) + T.dot(Whi, h_tm1) + bi)
            ct1 = T.tanh(T.dot(Wxj, x_t) + T.dot(Whj, h_tm1) + bj)
            ft = T.tanh(T.dot(Wxk, x_t) + T.dot(Whk, h_tm1) + bk)

            ct2 = T.tanh(it*ct1+ft*c_tm1)
            ot = T.tanh(T.dot(Wxo,x_t)+T.dot(Who,h_tm1) + bo)
            ht = ot*T.tanh(ct2)
            return ht

        rep, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0],
                                n_steps=X.shape[0])
        return rep


    def hinge_obj(self,R_1,R_2,P_1,P_2):
        zero = theano.shared(0.)
        hinge1 = 1-T.dot(R_1[-1],R_2[-1])+T.dot(R_1[-1],P_1[-1])
        hinge2 = 1-T.dot(R_1[-1],R_2[-1])+T.dot(R_2[-1],P_2[-1])
        d1 = ifelse(T.lt(hinge1,zero), zero, hinge1)
        d2 = ifelse(T.lt(hinge2,zero), zero, hinge2)
        return d1 + d2

    def adaup_step(self,p_ada,pg):
        return p_ada + pg**2

    def ada_step(self, p, p_ada, pg, eta):
        return p - eta*pg/(1E-4 + T.sqrt(p_ada))

    def __init__(self, We_initial, D):

        initial_We = theano.shared(We_initial)

        (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0) = self.initWeights(D, We_initial)
        self.params = (We, Wxi,Whi,bi, Wxj,Whj,bj, Wxk,Whk,bk, Wxo,Who,bo, h0,c0)

        (We_ada, Wxi_ada,Whi_ada,bi_ada,h0i_ada, Wxj_ada,Whj_ada,bj_ada,h0j_ada, Wxk_ada,
                Whk_ada,bk_ada,h0k_ada, Wxo_ada, Who_ada, bo_ada) = self.initAda(self.params)
        self.adas = (We_ada, Wxi_ada,Whi_ada,bi_ada,h0i_ada, Wxj_ada,Whj_ada,bj_ada,h0j_ada, Wxk_ada,
                Whk_ada,bk_ada,h0k_ada, Wxo_ada, Who_ada, bo_ada)

        #feed forward
        I = T.ivector()
        self.feedforwardf = theano.function(inputs = [I], outputs = self.rnn_layer(I,self.params))

        #cost and gradient
        I_1 = T.ivector(); I_2 = T.ivector(); I_3 = T.ivector(); I_4 = T.ivector()
        L_We = T.dscalar(); L_P = T.dscalar()

        R_1 = self.rnn_layer(I_1,self.params); R_2 = self.rnn_layer(I_2,self.params)
        P_1 = self.rnn_layer(I_3,self.params); P_2 = self.rnn_layer(I_4,self.params)

        cost = self.hinge_obj(R_1,R_2,P_1,P_2) + L_We/2.*T.sum((We-initial_We)**2)
        self.total_costf = theano.function(inputs = [I_1,I_2,I_3,I_4,L_P,L_We], outputs = cost)
        self.gradf = theano.function(inputs=[I_1,I_2,I_3,I_4,L_P,L_We], outputs = T.grad(cost, self.params))

        #updates
        eta = T.dscalar()
        gWe = T.dmatrix();
        gWxi = T.dmatrix(); gWhi = T.dmatrix(); gbi = T.dvector(); gh0i = T.dvector()
        gWxj = T.dmatrix(); gWhj = T.dmatrix(); gbj = T.dvector(); gh0j = T.dvector()
        gWxk = T.dmatrix(); gWhk = T.dmatrix(); gbk = T.dvector(); gh0k = T.dvector()
        gWxo = T.dmatrix(); gWho = T.dmatrix(); gbo = T.dvector();

        updatesAda=[
            (We_ada, self.adaup_step(We_ada, gWe)),

            (Wxi_ada, self.adaup_step(Wxi_ada, gWxi)),
            (Whi_ada, self.adaup_step(Whi_ada, gWhi)),
            (bi_ada, self.adaup_step(bi_ada, gbi)),
            (h0i_ada, self.adaup_step(h0i_ada, gh0i)),

            (Wxj_ada, self.adaup_step(Wxj_ada, gWxj)),
            (Whj_ada, self.adaup_step(Whj_ada, gWhj)),
            (bj_ada, self.adaup_step(bj_ada, gbj)),
            (h0j_ada, self.adaup_step(h0j_ada, gh0j)),

            (Wxk_ada, self.adaup_step(Wxk_ada, gWxk)),
            (Whk_ada, self.adaup_step(Whk_ada, gWhk)),
            (bk_ada, self.adaup_step(bk_ada, gbk)),
            (h0k_ada, self.adaup_step(h0k_ada, gh0k)),

            (Wxo_ada, self.adaup_step(Wxo_ada, gWxo)),
            (Who_ada, self.adaup_step(Who_ada, gWho)),
            (bo_ada, self.adaup_step(bo_ada, gbo))
        ]
        updates=[
            (We, self.ada_step(We, We_ada, gWe, eta)),

            (Wxi, self.ada_step(Wxi, Wxi_ada, gWxi, eta)),
            (Whi, self.ada_step(Whi, Whi_ada, gWhi, eta)),
            (bi, self.ada_step(bi, bi_ada, gbi, eta)),
            (h0i, self.ada_step(h0i, h0i_ada, gh0i, eta)),

            (Wxj, self.ada_step(Wxj, Wxj_ada, gWxj, eta)),
            (Whj, self.ada_step(Whj, Whj_ada, gWhj, eta)),
            (bj, self.ada_step(bj, bj_ada, gbj, eta)),
            (h0j, self.ada_step(h0j, h0j_ada, gh0j, eta)),

            (Wxk, self.ada_step(Wxk, Wxk_ada, gWxk, eta)),
            (Whk, self.ada_step(Whk, Whk_ada, gWhk, eta)),
            (bk, self.ada_step(bk, bk_ada, gbk, eta)),
            (h0k, self.ada_step(h0k, h0k_ada, gh0k, eta)),

            (Wxo, self.ada_step(Wxo, Wxo_ada, gWxo, eta)),
            (Who, self.ada_step(Who, Who_ada, gWho, eta)),
            (bo, self.ada_step(bo, bo_ada, gbo, eta))
        ]
        self.updateAdaf = theano.function(inputs=[gWe,gWxi,gWhi,gbi,gh0i, gWxj,gWhj,gbj,gh0j,
                                                  gWxk,gWhk,gbk,gh0k, gWxo,gWho,gbo], updates=updatesAda)
        self.updatef = theano.function(inputs=[gWe,gWxi,gWhi,gbi,gh0i, gWxj,gWhj,gbj,gh0j,
                                                  gWxk,gWhk,gbk,gh0k, gWxo,gWho,gbo], updates=updates)

    def feedforward(self, xx):
        repr = self.feedforwardf(xx)
        return repr[-1]

    def feedforwardAll(self, batch):
        for i in batch:
            t1 = i[0]
            t2 = i[1]
            #print t1.embeddings
            t1.representation = self.feedforward(t1.embeddings)
            t2.representation = self.feedforward(t2.embeddings)

    def getVectorFromIndices(self, idx1, idx2):
        r1 = self.feedforward(idx1)
        r2 = self.feedforward(idx2)
        return r1,r2

    def updateBatchParams(self,d1,d2,p1,p2,eta,lamWe):
        gg = (0., 0.,0.,0.,0.,0., 0.,0.,0.,0.,0., 0.,0.,0.,0.,0., 0.,0.,0.)
        for i in range(len(d1)):
            gg = map(sum, zip(gg, self.gradf(d1[i],d2[i],p1[i],p2[i],lamWe)))
            #gg = self.grad_one(d1,d2,p1,p2)
        gg = map(lambda a: a/len(d1), gg)
        #print gg
        self.updateAdaf(gg[0],gg[1],gg[2],gg[3],gg[4],gg[5],gg[6],gg[7],gg[8],gg[8],
                        gg[10],gg[11],gg[12],gg[13],gg[14],gg[15])
        self.updatef(gg[0],gg[1],gg[2],gg[3],gg[4],gg[5],gg[6],gg[7],gg[8],gg[8],
                        gg[10],gg[11],gg[12],gg[13],gg[14],gg[15],eta)

    def getBatchCost(self,d1,d2,p1, p2, lamC, lamWe):
        cost = 0
        for i in range(len(d1)):
            cost += self.total_costf(d1[i], d2[i], p1[i], p2[i], lamWe)
        return cost/len(d1)
