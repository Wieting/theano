
from utils import getWordmap
from params import params
from utils import getData
from theano_model_dropout import theano_model_dropout
from utils import getPairsBatch
from utils import getTuplesFromExamples
from random import shuffle
from adagrad import adagrad
from evaluate import evaluate_adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)
D = 25
lamWe = 1E-5
eta = 0.01
type = "MAX"
skipwords = '../data/skipwiki25.txt'
parawords = '../data/paragram_vectors.txt'
(words, We) = getWordmap(skipwords)
params = params()
params.lamWe = lamWe

params.batchsize=100

adagrad(params, We, D, words, theano_model_dropout(We,D,0.8))