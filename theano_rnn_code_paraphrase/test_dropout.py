import theano
import numpy as np
from theano import tensor as T
import pdb
from theano.ifelse import ifelse
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

p=0.5
rng = np.random.RandomState()
srng = RandomStreams(rng.randint(99999))


mask = srng.binomial(n=1, p=1-p, size=(2,2))
mask = T.cast(mask, theano.config.floatX)
m = np.array([[2,3],[5,6]])
m = theano.shared(m)
x = m*mask
print x.eval()
x = m*mask
print x.eval()
x = m*mask
print x.eval()
x = m*mask
print x.eval()