import numpy as np
from random import shuffle
from utils import getPairsBatch
from utils import getPairs
from evaluate import evaluate_adagrad
from theano_model import theano_model
from utils import getPairsBatch
from utils import getTuplesFromExamples
from utils import getTuplesFromExamples
import sys
import random

random.seed(1)
np.random.seed(1)

def checkIfQuarter(idx,n,batchsize):
    start = idx - batchsize
    for i in range(start,idx):
        if i==round(n/4.) or i==round(n/2.) or i==round(3*n/4.):
            return True
    return False

def evaluateParams(words,We):
    evaluate_adagrad(We,words)

def adagrad(params, We, D, words, tm):

    data = params.data
    n = len(data)
    counter = 0

    print params.lamC, params.lamWe
    for ep in range(params.epochs):
        shuffle(data)
        cost = tm.getBatchCost(data,params.lamWe)
        print "cost at epoch at "+str(ep)+" : "+str(cost)
        idx = 0
        while idx < n:
            batch = data[idx: idx + params.batchsize if idx + params.batchsize < len(data) else len(data)]
            idx += params.batchsize
            tm.updateBatchParams(batch,params.lamWe,params.eta)
            if(checkIfQuarter(idx,n,params.batchsize)):
                if(params.save):
                    counter += 1
                    #saveParams(tm,params.outfile,counter)
                if(params.evaluate):
                    evaluateParams(words,tm)
                    sys.stdout.flush()
        if(params.save):
            counter += 1
            #saveParams(words,We,params.outfile,counter)
        if(params.evaluate):
            evaluateParams(words,tm)
            sys.stdout.flush()
