
import theano
import numpy as np
from theano import tensor as T
from collections import OrderedDict
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

class theano_model_dropout(object):

    def initWeights(self, D, We_initial) :
        We = theano.shared(We_initial)
        Wx = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Wh = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        b = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))
        h0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wx_l = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Wh_l = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        b_l = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))

        Wf = theano.shared(np.random.uniform(low=-np.sqrt(6. / (5 + D)), high=np.sqrt(6. / (5 + D)), size=(5,D)))
        bf = theano.shared(np.random.uniform(low=-np.sqrt(6. / (5 + D)), high=np.sqrt(6. / (5 + D)), size=(5)))
        return  [We,Wx,Wh,b,h0,Wx_l,Wh_l,b_l,Wf,bf]

    def rnn_layer(self, I, params, p):
        [We,Wx,Wh,b,h0,Wx_l,Wh_l,b_l,Wf,bf] = params
        X = We[I]
        mask = self.srng.binomial(n=1, p=1-p, size=b.shape)
        mask = T.cast(mask, theano.config.floatX)
        def recurrence(x_t, h_tm1):
            representation = T.tanh(T.dot(Wx, x_t)*mask + T.dot(Wh, h_tm1) + b)
            return representation

        rep, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0],
                                n_steps=X.shape[0])
        return rep[-1]


    def hinge_obj(self,R_1,R_2,params,y, p):
        [We,Wx,Wh,b,h0,Wx_l,Wh_l,b_l,Wf,bf] = params
        mask = self.srng.binomial(n=1, p=1-p, size=b_l.shape)
        mask = T.cast(mask, theano.config.floatX)
        score = T.nnet.sigmoid(T.dot(Wx_l,R_1*R_2) + T.dot(Wh_l,T.abs_(R_1-R_2)) + b_l)
        #mask = self.srng.binomial(n=1, p=1-p, size=bf.shape)
        #mask = T.cast(mask, theano.config.floatX)
        score = T.nnet.softmax(T.dot(Wf,score)+bf)
        score = T.dot(score,self.r)[0]
        return (score-y)**2

    def regression(self,R_1,R_2,params):
        [We,Wx,Wh,b,h0,Wx_l,Wh_l,b_l,Wf,bf] = params
        score = T.nnet.sigmoid(T.dot(Wx_l,R_1*R_2) + T.dot(Wh_l,T.abs_(R_1-R_2)) + b_l)
        score = T.nnet.softmax(T.dot(Wf,score)+bf)
        score = T.dot(score,self.r)[0]
        return score

    def __init__(self, We_initial, D, drop_rate):

        self.drop_rate = drop_rate

        self.rng = np.random.RandomState(1); self.srng = RandomStreams(self.rng.randint(99999))
        self.r = theano.shared(np.array([1,2,3,4,5])) #used for sim

        #params
        initial_We = theano.shared(We_initial)
        self.params = self.initWeights(D, We_initial)
        [We,Wx,Wh,b,h0,Wx_l,Wh_l,b_l,Wf,bf] = self.params

        #grads
        gWe = T.dmatrix(); gWx = T.dmatrix(); gWh = T.dmatrix(); gWx_l = T.dmatrix(); gWh_l = T.dmatrix()
        gb = T.dvector(); gh0 = T.dvector(); gb_l = T.dvector(); gWf = T.dmatrix(); gbf = T.dvector()
        self.gparams = [gWe,gWx,gWh,gb,gh0,gWx_l,gWh_l,gb_l,gWf,gbf]

        #feed forward
        I = T.ivector(); p = T.dscalar(); y = T.dscalar()
        self.feedforwardf = theano.function(inputs = [I,p], outputs = self.rnn_layer(I,self.params,p))

        #cost and gradient
        I_1 = T.ivector(); I_2 = T.ivector()
        L_We = T.dscalar()
        R_1 = self.rnn_layer(I_1,self.params, p); R_2 = self.rnn_layer(I_2,self.params, p)
        cost1 = self.hinge_obj(R_1,R_2,self.params,y,p)
        cost2 = L_We/2.*T.sum((We-initial_We)**2)
        regression = self.regression(R_1,R_2,self.params)
        self.costf_1 = theano.function(inputs = [I_1,I_2,y,p], outputs = cost1)
        self.costf_2 = theano.function(inputs = [L_We], outputs = cost2)
        self.regressionf = theano.function(inputs = [I_1,I_2,p], outputs = regression)

        self.gradf1 = theano.function(inputs=[I_1,I_2,y,p], outputs = T.grad(cost1, self.params))
        self.gradf2 = theano.function(inputs=[L_We], outputs = T.grad(cost2, We))

        #updates
        eta = T.dscalar()

        updates = OrderedDict()
        for p,gp in zip(self.params,self.gparams):
            #agrad = theano.shared(np.zeros(p.get_value().shape, dtype=theano.config.floatX))
            #agrad_new = agrad + gp*gp
            #updates[agrad] = agrad_new
            updates[p]=p-(eta/T.sqrt(agrad_new+1E-6))*gp
            #updates[p]=p-eta*gp

        self.updatesf = theano.function(inputs=self.gparams+[eta], updates=updates)

    def feedforward(self, xx):
        repr = self.feedforwardf(xx,0.)
        return repr[-1]

    def feedforwardAll(self, batch):
        for i in batch:
            t1 = i[0]
            t2 = i[1]
            #print t1.embeddings
            t1.representation = self.feedforward(t1.embeddings)
            t2.representation = self.feedforward(t2.embeddings)

    def getVectorFromIndices(self, idx1, idx2):
        r1 = self.feedforward(idx1)
        r2 = self.feedforward(idx2)
        return r1,r2

    def updateBatchParams(self,data,lamWe,eta):
        #for i in range(len(data)):
        #    print self.regression(data[i][0],data[i][1],0.)
        gg = [0.]*len(self.params)
        for i in range(len(data)):
            gg = map(sum, zip(gg, self.gradf1(data[i][0],data[i][1],data[i][2],self.drop_rate)))
        gg = map(lambda a: a/len(data), gg)
        gg[0] += self.gradf2(lamWe)
        args = gg + [eta]
        self.updatesf(*args)

    def getBatchCost(self,data,lamWe):
        cost = 0
        for i in range(len(data)):
            cost += self.costf_1(data[i][0], data[i][1], data[i][2], 0.)
        return cost/len(data) + self.costf_2(lamWe)
