
from utils import getWordmap
from params import params
from theano_model_dropout import theano_model_dropout
from utils import loadSE
from utils import convertTextToTuples
from adagrad import adagrad
import random
import numpy as np

random.seed(1)
np.random.seed(1)

D = 25
lamWe = 1
eta = 0.2
skipwords = '../data/skipwiki25.txt'
parawords = '../data/paragram_vectors.txt'
(words, We) = getWordmap(parawords)

params = params()
params.lamWe = lamWe
params.batchsize=25
params.eta = eta
model = theano_model_dropout(We, D, 0.5)

(t1,t2,y,ents) = loadSE('../dataset/SICK_train.txt')
(d1,d2) = convertTextToTuples(t1, t2, words)

data = []
for i in range(len(d1)):
    data.append((d1[i],d2[i],y[i]))
    #print d1[i], d2[i],y[i], t1[i], t2[i]
params.data = data
#print model.total_costf(data[0][0], data[0][1], data[0][2], lamWe, 0.)
adagrad(params, We, D, words, model)
#evaluate_adagrad(model,words)