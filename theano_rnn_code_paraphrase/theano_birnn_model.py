
import theano
import numpy as np
from theano import tensor as T
import pdb
from theano.ifelse import ifelse

class theano_birnn_model(object):

    def initWeights(self, D, We_initial) :
        We = theano.shared(We_initial)
        Wx = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Wh = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        b = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))
        h0 = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D)))
        Wf = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        Wb = theano.shared(np.random.uniform(low=-np.sqrt(6. / (D + D)), high=np.sqrt(6. / (D + D)), size=(D, D)))
        return  (We,Wx,Wh,b,h0,Wb,Wf)

    def initAda(self, params):
        (We,Wx,Wh,b,h0,Wf,Wb) = params
        We_ada = theano.shared(value=np.zeros(We.get_value().shape, dtype=theano.config.floatX))
        Wx_ada = theano.shared(value=np.zeros(Wx.get_value().shape, dtype=theano.config.floatX))
        Wh_ada = theano.shared(value=np.zeros(Wh.get_value().shape, dtype=theano.config.floatX))
        b_ada = theano.shared(value=np.zeros(b.get_value().shape, dtype=theano.config.floatX))
        h0_ada = theano.shared(value=np.zeros(h0.get_value().shape, dtype=theano.config.floatX))
        Wf_ada = theano.shared(value=np.zeros(Wx.get_value().shape, dtype=theano.config.floatX))
        Wb_ada = theano.shared(value=np.zeros(Wh.get_value().shape, dtype=theano.config.floatX))
        return (We_ada,Wx_ada,Wh_ada,b_ada,h0_ada,Wf_ada,Wb_ada)

    def rnn_layer(self, I, params):
        (We,Wx,Wh,b,h0,Wf,Wb) = params
        X = We[I]
        def recurrence(x_t, h_tm1):
            representation = T.tanh(T.dot(Wx, x_t) + T.dot(Wh, h_tm1) + b)
            return representation

        rep1, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0],
                                n_steps=X.shape[0])

        rep2, _ = theano.scan(fn=recurrence,
                                sequences=X,
                                outputs_info=[h0],
                                n_steps=X.shape[0], go_backwards=True)


        return T.tanh(T.dot(Wf,rep1[-1]) + T.dot(Wb,rep2[-1]) + b)


    def hinge_obj(self,R_1,R_2,P_1,P_2):
        zero = theano.shared(0.)
        hinge1 = 1-T.dot(R_1,R_2)+T.dot(R_1,P_1)
        hinge2 = 1-T.dot(R_1,R_2)+T.dot(R_2,P_2)
        d1 = ifelse(T.lt(hinge1,zero), zero, hinge1)
        d2 = ifelse(T.lt(hinge2,zero), zero, hinge2)
        return d1 + d2


    def L2(self,L2_reg, w1,w2,w3,w4,w5,w6):
        return L2_reg/2. * ((w1 ** 2).sum() + (w2 ** 2).sum() + (w3 ** 2).sum() + (w4 ** 2).sum()
                            + (w5 ** 2).sum() + (w6 ** 2).sum())

    def adaup_step(self,p_ada,pg):
        return p_ada + pg**2

    def ada_step(self, p, p_ada, pg, eta):
        return p - eta*pg/(1E-4 + T.sqrt(p_ada))

    def __init__(self, We_initial, D):

        initial_We = theano.shared(We_initial)
        (We,Wx,Wh,b,h0,Wf,Wb) = self.initWeights(D, We_initial)
        self.params = (We,Wx,Wh,b,h0,Wf,Wb)
        (We_ada,Wx_ada,Wh_ada,b_ada,h0_ada,Wf_ada,Wb_ada) = self.initAda(self.params)
        self.adas = (We_ada,Wx_ada,Wh_ada,b_ada,h0_ada,Wf_ada,Wb_ada)

        #feed forward
        I = T.ivector()
        self.feedforwardf = theano.function(inputs = [I], outputs = self.rnn_layer(I,self.params))

        #cost and gradient
        I_1 = T.ivector(); I_2 = T.ivector(); I_3 = T.ivector(); I_4 = T.ivector()
        L_We = T.dscalar(); L_P = T.dscalar()

        R_1 = self.rnn_layer(I_1,self.params); R_2 = self.rnn_layer(I_2,self.params)
        P_1 = self.rnn_layer(I_3,self.params); P_2 = self.rnn_layer(I_4,self.params)

        cost = self.hinge_obj(R_1,R_2,P_1,P_2) + self.L2(L_P,Wx,Wh,b,h0,Wf,Wb) +  L_We/2.*T.sum((We-initial_We)**2)
        self.total_costf = theano.function(inputs = [I_1,I_2,I_3,I_4,L_P,L_We], outputs = cost)
        self.gradf = theano.function(inputs=[I_1,I_2,I_3,I_4,L_P,L_We], outputs = T.grad(cost, self.params))

        #updates
        eta = T.dscalar()
        gWe = T.dmatrix(); gWx = T.dmatrix(); gWh = T.dmatrix()
        gb = T.dvector(); gh0 = T.dvector(); gWb = T.dmatrix(); gWf = T.dmatrix()
        updatesAda=[
            (We_ada, self.adaup_step(We_ada, gWe)),
            (Wx_ada, self.adaup_step(Wx_ada, gWx)),
            (Wh_ada, self.adaup_step(Wh_ada, gWh)),
            (b_ada, self.adaup_step(b_ada, gb)),
            (h0_ada, self.adaup_step(h0_ada, gh0)),
            (Wf_ada, self.adaup_step(Wf_ada, gWf)),
            (Wb_ada, self.adaup_step(Wb_ada, gWb))
        ]
        updates=[
            (We, self.ada_step(We, We_ada, gWe, eta)),
            (Wx, self.ada_step(Wx, Wx_ada, gWx, eta)),
            (Wh, self.ada_step(Wh, Wh_ada, gWh, eta)),
            (b, self.ada_step(b, b_ada, gb, eta)),
            (h0, self.ada_step(h0, h0_ada, gh0, eta)),
            (Wf, self.ada_step(Wf, Wf_ada, gWf, eta)),
            (Wb, self.ada_step(Wb, Wb_ada, gWb, eta))
        ]
        self.updateAdaf = theano.function(inputs=[gWe,gWx,gWh,gb,gh0,gWf,gWb], updates=updatesAda)
        self.updatef = theano.function(inputs=[gWe,gWx,gWh,gb,gh0,eta,gWf,gWb], updates=updates)

    def feedforward(self, xx):
        repr = self.feedforwardf(xx)
        return repr

    def feedforwardAll(self, batch):
        for i in batch:
            t1 = i[0]
            t2 = i[1]
            #print t1.embeddings
            t1.representation = self.feedforward(t1.embeddings)
            t2.representation = self.feedforward(t2.embeddings)

    def getVectorFromIndices(self, idx1, idx2):
        r1 = self.feedforward(idx1)
        r2 = self.feedforward(idx2)
        return r1,r2

    def updateBatchParams(self,d1,d2,p1,p2,eta,lamC,lamWe):
        gg = (0.,0.,0.,0.,0.,0.,0.)
        for i in range(len(d1)):
            gg = map(sum, zip(gg, self.gradf(d1[i],d2[i],p1[i],p2[i],lamC,lamWe)))
            #gg = self.grad_one(d1,d2,p1,p2)
        gg = map(lambda a: a/len(d1), gg)
        #print gg
        self.updateAdaf(gg[0],gg[1],gg[2],gg[3],gg[4],gg[5],gg[6])
        self.updatef(gg[0],gg[1],gg[2],gg[3],gg[4],eta,gg[5],gg[6])

    def getBatchCost(self,d1,d2,p1, p2, lamC, lamWe):
        cost = 0
        for i in range(len(d1)):
            cost += self.total_costf(d1[i], d2[i], p1[i], p2[i], lamC, lamWe)
        return cost/len(d1)
